class SearchesController < ApplicationController
  def index
    parser = PhoneParser::Phonearena.new(params[:search][:phone_name])
    @dimensions = parser.data[:dimensions]
  end
end
