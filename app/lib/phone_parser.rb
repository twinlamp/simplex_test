require 'httparty'

class PhoneParser
  def initialize(phone)
    @phone = phone
  end

  def data
    {
      dimensions: dimensions
    }
  end

  def dimensions; end

  private

  def phone_page
    @phone_page ||= Nokogiri::HTML(HTTParty.get(phone_url).body)
  end

  def search_page
    Nokogiri::HTML(HTTParty.get(search_url).body)
  end

  def search_url
    raise 'Method is abstract: search_url'
  end

  def phone_url
    raise 'Method is abstract: phone_url'
  end
end
