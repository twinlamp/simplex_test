class PhoneParser::Phonearena < PhoneParser
  BASE_URL = 'http://www.phonearena.com'

  def dimensions
    return unless phone_url
    phone_page.at('#design_cPoint li:contains("Dimensions:") ul > li')&.text
  end

  private

  def search_url
    BASE_URL + '/search?term=' + URI.escape(@phone.to_s)
  end

  def phone_url
    link_el = search_page.at('#phones .s_listing > div:first-child a[href^="/phones"]')
    return unless link_el
    BASE_URL + link_el.attributes['href'].value
  end
end
