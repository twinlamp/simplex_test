require 'rails_helper'

describe SearchesController do
  describe 'GET index' do
    before(:each) do
      allow_any_instance_of(PhoneParser::Phonearena).to receive(:dimensions).and_return('123')
    end
    it 'assigns parser value to @dimensions' do
      get :index, xhr: true, params: { search: { phone_name: 'Galaxy S9' } }
      expect(assigns(:dimensions)).to eq('123')
    end
    it 'renders index template' do
      get :index, xhr: true, params: { search: { phone_name: 'Galaxy S9' } }
      expect(response).to render_template(:index)
    end
  end
end
