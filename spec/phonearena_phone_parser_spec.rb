require 'rails_helper'

describe 'PhoneParser::Phonearena.dimensions' do
  context 'phone found' do
    context 'dimensions not found' do
      it 'returns nil' do
        parser = PhoneParser::Phonearena.new('LAVA A9')
        VCR.use_cassette('lava_a9') do
          expect(parser.dimensions).to eq(nil)
        end
      end
    end
    context 'dimensions found' do
      it 'returns dimensions' do
        parser = PhoneParser::Phonearena.new('Galaxy S9')
        VCR.use_cassette('galaxy_s9') do
          expect(parser.dimensions).to eq('6.21 x 2.91 x 0.33 inches  (157.7 x 73.8 x 8.5 mm)')
        end
      end
    end
  end
  context 'phone not found' do
    it 'returns nil' do
      parser = PhoneParser::Phonearena.new('fdfsdafsdf')
      VCR.use_cassette('fdfsdafsdf') do
        expect(parser.dimensions).to eq(nil)
      end
    end
  end
  context 'phone nil' do
    it 'returns nil' do
      parser = PhoneParser::Phonearena.new(nil)
      VCR.use_cassette('nil') do
        expect(parser.dimensions).to eq(nil)
      end
    end
  end    
end
